package com.javatpoint.mypackage;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import java.util.*;
public class Main {

	public static void main(String[] args) {
		Configuration c = new Configuration();
		c.configure("hibernate.cfg.xml");
		SessionFactory sessionfactory = c.buildSessionFactory();
		Session session = sessionfactory.openSession();
		Transaction transaction = session.beginTransaction();
		User u=new User();
		u.setName("Revathi");
		Account account1=new Account();
		account1.setTypeofaccount("savings");
			Account account2=new Account();
		account2.setTypeofaccount("term");
		account1.setUser(u);
		account2.setUser(u);
		List<Account> listofaccount=new LinkedList<>();
		listofaccount.add(account1);
		listofaccount.add(account2);
		u.setAccount(listofaccount);
		session.save(u);
		transaction.commit();
	System.out.println("successful");
		
	}

}
