package com.javatpoint.mypackage;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="userdetails2")
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
	public List<Account> getAccount() {
		return account;
	}
	public void setAccount(List<Account> account) {
		this.account = account;
	}
	@Column(name="username")
private String name;
	@OneToMany(cascade=CascadeType.ALL,mappedBy="user")
	private List<Account> account;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

}
