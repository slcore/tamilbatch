package com.javatpoint.mypackage;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;



public class EmpDetailMain {
public static void main(String[] args) {
	/*Configuration config=new Configuration();
	config.configure("hibernate.cfg.xml");
	SessionFactory sf=config.buildSessionFactory();
	Session session=sf.openSession();*/
	Session session = new Configuration().configure("hibernate.cfg.xml")
            .buildSessionFactory().openSession();
	EmployeeDetails empdetails=new EmployeeDetails();
	empdetails.setUserName("revathi");
	empdetails.setPassword("revathi");//transient
	session.save(empdetails);//persistent
	session.close();//detached
	//tx.commit();
		Query query=session.createSQLQuery("select * from tbl_empdetails").addEntity(EmployeeDetails.class);
		List<EmployeeDetails> ls=query.list();
		ls.forEach(emp->System.out.println(emp.getPassword()));
	/*Query query=session.getNamedQuery("findEmployeeByName");
	query.setParameter("name","revathi");
	List<EmployeeDetails> ls=query.list();
	ls.forEach(emp->System.out.println(emp.getUserName()));*/
	/*Transaction tx=session.beginTransaction();
	EmployeeDetails empdetails=new EmployeeDetails();
	empdetails.setUserName("revathi");
	empdetails.setPassword("revathi");
	session.save(empdetails);
	tx.commit();*/
	
	
}

}
