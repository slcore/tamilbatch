package com.javatpoint.mypackage;

import javax.persistence.*;

@Entity
@Table(name="dept")
@NamedQueries({@NamedQuery(name="fecthAllDepartment" ,query="from Department")})
public class Department {
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
@Column(name="empName",length=30)
private String name;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
}
