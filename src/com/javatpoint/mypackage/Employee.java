package com.javatpoint.mypackage;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name= "emp500") 
@NamedQueries({@NamedQuery(name="fecthAllDepartment" ,query="from Department")})
public class Employee implements Serializable {  
	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;  
	@Column(name="FN",length=10,nullable=false)
	private String firstName;
	private String lastName;  
@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}


  
public int getId() {  
    return id;  
}  
public void setId(int id) {  
    this.id = id;  
}  
public String getFirstName() {  
    return firstName;  
}  
public void setFirstName(String firstName) {  
    this.firstName = firstName;  
}  
public String getLastName() {  
    return lastName;  
}  
public void setLastName(String lastName) {  
    this.lastName = lastName;  
}  
}  