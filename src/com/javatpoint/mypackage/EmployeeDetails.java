package com.javatpoint.mypackage;

import javax.persistence.*;

import org.hibernate.annotations.GeneratorType;
/*@NamedQueries(  
	    {  
	        @NamedQuery(  
	        name = "findEmployeeByName",  
	        query = "from EmployeeDetails e where e.userName = :name"  
	        )  
	    }  
	) */
@Entity
@Table(name="tbl_empdetails")
public class EmployeeDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
private int empId;
	@Column(name="name",length=5,nullable=false)
	private String userName;
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	private String password;

}
