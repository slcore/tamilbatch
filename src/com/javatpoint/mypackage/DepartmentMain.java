package com.javatpoint.mypackage;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class DepartmentMain {
public static void main(String[] args) {
	Configuration c=new Configuration();
	c.configure("hibernate.cfg.xml");
	SessionFactory sessionfactory=c.buildSessionFactory();
Session session=	sessionfactory.openSession();
Query query=session.getNamedQuery("fecthAllDepartment");
List<Department> dept=query.list();
dept.forEach(department->System.out.println(department.getName()));

/*Transaction tx=session.beginTransaction();
Department dept=new Department();
dept.setName("EEE");
session.save(dept);
System.out.println("saved successfully");
tx.commit();*/
session.close();//evict()    clear()    close()
}

}
