package com.javatpoint.mypackage;

import javax.persistence.*;
@Entity
@Table(name = "tbl_emp4")
public class Emp {
	@Id
	@Column(name="EmpId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String empName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}

}
