package com.javatpoint.mypackage;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.*;
public class StudentMain {

	public static void main(String[] args) {
		Configuration c=new Configuration();
		c.configure("hibernate.cfg.xml");
		SessionFactory sessionfactory=c.buildSessionFactory();
	Session session=	sessionfactory.openSession();
	Criteria  cr=session.createCriteria(Student.class);
	/*cr.setFirstResult(0);
	cr.setMaxResults(2);*/
/*	cr.add(Restrictions.gt("id",1));*/
	cr.setProjection(Projections.property("name"));  
List<String> ls=	cr.list();
ls.forEach(student->System.out.println(student));
	//SQLQuery query=session.createSQLQuery("select * from student_details").addEntity(Student.class);
	//List<Student> ls=query.list();
	//ls.forEach(student->System.out.println(student.getEmail()+" "+student.getName()));
	/*for(Student student:ls)
	{
		System.out.println(student.getEmail()+" "+student.getName());
	}*/
	/*Transaction t=session.beginTransaction();
	Student st=new Student();
	st.setName("revathi");
	st.setEmail("revathi@hcl.com");
	session.save(st);*/
	//t.commit();
		
	}

}
