package com.javatpoint.mypackage;  
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
//http://www.srimanjavagroup.com/blog/50/hibernate-4-service-and-serviceregistry.htm
//https://kkjavatutorials.com/save-and-persist-an-entity-example-in-hibernate/
import org.hibernate.Session;  
import org.hibernate.SessionFactory;  
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
  
public class StoreData {  
public static void main(String[] args) {  
	Session session = new AnnotationConfiguration().configure().buildSessionFactory().openSession();
/*Transaction t = session.beginTransaction();
         
    Employee e1=new Employee();  
  //  e1.setId(101);  
    e1.setFirstName("Hcl");  
    e1.setLastName("technologies");  
      
    session.save(e1);*/
/*	Query query=session.createQuery("select e from Employee e ");//HQL
	List<Employee> ls=query.list();
	for(Employee e:ls){
		System.out.println(e);
	}*/
/*	SQLQuery query=session.createSQLQuery("select * from emp500").addEntity(Employee.class);//NativeSQL
	List<Employee> ls=query.list();
	for(Employee e:ls){
		System.out.println(e);
	}*/
	//t.commit();
	/*Criteria c=session.createCriteria(Employee.class);  
	c.add(Restrictions.gt("id",2));*/
	//Criteria c=session.createCriteria(Employee.class);  
	//c.addOrder(Order.desc("id"));  
	Criteria c=session.createCriteria(Employee.class);  
	c.setProjection(Projections.property("firstName"));  
	List<String> list=c.list();  
	for(String e:list){
		System.out.println(e);
	}
	//salary is the propertyname  
	/*Criteria c=session.createCriteria(Employee.class);  
	c.setFirstResult(1);  
	c.setMaxResults(2);  
	List<Employee> list=c.list();  
	for(Employee e:list){
		System.out.println(e);
	}*/
    System.out.println("successfully saved");  
	   session.close();  
      
}  
}  