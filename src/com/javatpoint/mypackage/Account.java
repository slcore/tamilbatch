package com.javatpoint.mypackage;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="accountdetails2")
public class Account implements Serializable{
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int accno;
private String typeofaccount;
@ManyToOne(cascade=CascadeType.ALL)
private User user;

public User getUser() {
	return user;
}
public void setUser(User user) {
	this.user = user;
}
public int getAccno() {
	return accno;
}
public void setAccno(int accno) {
	this.accno = accno;
}
public String getTypeofaccount() {
	return typeofaccount;
}
public void setTypeofaccount(String typeofaccount) {
	this.typeofaccount = typeofaccount;
}


}
